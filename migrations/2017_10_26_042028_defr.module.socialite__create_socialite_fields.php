<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Anomaly\UsersModule\User\UserModel;
use Defr\SocialiteModule\Provider\ProviderModel;

class DefrModuleSocialiteCreateSocialiteFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name'          => 'anomaly.field_type.text',
        'config'        => 'anomaly.field_type.textarea',
        'enabled'       => 'anomaly.field_type.boolean',
        'icon'          => 'anomaly.field_type.file',
        'slug'          => [
            'type'   => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type'    => '_',
            ],
        ],
        'user'          => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => UserModel::class,
            ],
        ],
        'provider'      => [
            'type' => 'anomaly.field_type.relationship',
            'config' => [
                'related' => ProviderModel::class,
            ],
        ],
    ];

}
