<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleSocialiteCreateAccountsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'accounts',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'user',
        'provider',
        'config',
    ];

}
