<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleSocialiteCreateProvidersStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'providers',
        'title_column' => 'name',
        'sortable'     => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'          => [
            'required' => true,
        ],
        'slug'          => [
            'unique' => true,
            'required' => true,
        ],
        'icon',
        'config',
        'enabled',
    ];

}
