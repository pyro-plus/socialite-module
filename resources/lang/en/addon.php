<?php

return [
    'title'       => 'Socialite',
    'name'        => 'Socialite Module',
    'description' => 'Social Login for PyroCMS and Streams Platform',
    'section'     => [
        'providers' => 'Providers',
        'accounts'  => 'Accounts',
    ],
];
