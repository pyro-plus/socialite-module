<?php

return [
    'name'     => [
        'name' => 'Name',
    ],
    'slug'     => [
        'name' => 'Slug',
    ],
    'enabled'  => [
        'name' => 'Enabled',
    ],
    'user'     => [
        'name' => 'User',
    ],
    'icon'     => [
        'name' => 'Icon',
    ],
    'provider' => [
        'name' => 'Provider',
    ],
    'config'   => [
        'name' => 'Config',
    ],
];
