<?php namespace Defr\SocialiteModule\Account\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Defr\SocialiteModule\Provider\Contract\ProviderInterface;

interface AccountRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Find account by provider
     *
     * @param  UserInterface      $user      The user
     * @param  ProviderInterface  $provider  The provider
     */
    public function findByUserAndProvider(
        UserInterface $user,
        ProviderInterface $provider
    );

}
