<?php namespace Defr\SocialiteModule\Account\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface AccountInterface extends EntryInterface
{

    /**
     * Gets the user.
     *
     * @return  UserInterface  The user.
     */
    public function getUser();

    /**
     * Gets the provider.
     *
     * @return  ProviderInterface  The provider.
     */
    public function getProvider();

}
