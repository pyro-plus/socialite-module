<?php namespace Defr\SocialiteModule\Account\Command;

use Anomaly\UsersModule\Role\Contract\RoleRepositoryInterface;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;

class FindOrCreateUser
{

    /**
     * User data
     *
     * @var mixed
     */
    protected $data;

    /**
     * Create an instance of FindOrCreateUser class
     *
     * @param  mixed  $data  The data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Handle the command
     *
     * @param   UserRepositoryInterface  $users  The users
     * @param   RoleRepositoryInterface  $roles  The roles
     * @return  UserInterface
     */
    public function handle(
        UserRepositoryInterface $users,
        RoleRepositoryInterface $roles
    )
    {
        if ($user = $users->findByEmail($this->data->getEmail())) {
            return $user;
        }

        $user = $users->create([
            'email'        => $this->data->getEmail(),
            'username'     => $this->data->getNickname(),
            'display_name' => $this->data->getName(),
            'password'     => bcrypt(str_random(16)),
            'activated'    => true,
            'enabled'      => true,
        ]);

        $user->attachRole($roles->findBySlug('user'));

        return $user;
    }

}
