<?php namespace Defr\SocialiteModule\Account\Command;

use Anomaly\UsersModule\User\Contract\UserInterface;
use Anomaly\UsersModule\User\UserAuthenticator;

class AuthenticateUser
{

    /**
     * User instance
     *
     * @var  UserInterface
     */
    protected $user;

    /**
     * Create an instance of AuthenticateUser class
     *
     * @param  UserInterface  $user  The user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the command
     *
     * @param  UserAuthenticator  $auth  The auth
     */
    public function handle(UserAuthenticator $auth)
    {
        $auth->login($this->user, true);
    }

}
