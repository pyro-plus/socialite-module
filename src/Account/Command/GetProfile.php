<?php namespace Defr\SocialiteModule\Account\Command;

use Defr\SocialiteModule\Provider\Contract\ProviderInterface;
use Laravel\Socialite\Facades\Socialite;

class GetProfile
{

    /**
     * Provider instance
     *
     * @var ProviderInterface
     */
    protected $provider;

    /**
     * Create an instance of GetProfile class
     *
     * @param  ProviderInterface  $provider  The provider
     */
    public function __construct(ProviderInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Handle the command
     *
     * @return Laravel\Socialite\Contracts\User
     */
    public function handle()
    {
        /* @var Laravel\Socialite\Contracts\Provider $driver */
        $driver = Socialite::driver($this->provider->getSlug());

        return $driver->stateless()->user();
    }

}
