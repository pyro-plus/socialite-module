<?php namespace Defr\SocialiteModule\Account;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Defr\SocialiteModule\Account\Contract\AccountRepositoryInterface;
use Defr\SocialiteModule\Provider\Contract\ProviderInterface;

class AccountRepository extends EntryRepository implements AccountRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var AccountModel
     */
    protected $model;

    /**
     * Create a new AccountRepository instance.
     *
     * @param AccountModel $model
     */
    public function __construct(AccountModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find account by provider
     *
     * @param   UserInterface      $user      The user
     * @param   ProviderInterface  $provider  The provider
     * @return  AccountInterface
     */
    public function findByUserAndProvider(
        UserInterface $user,
        ProviderInterface $provider
    )
    {
        return $this->model
            ->where('provider_id', $provider->getId())
            ->where('user_id', $user->getId())
            ->first();
    }

}
