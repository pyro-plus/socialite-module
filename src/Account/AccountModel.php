<?php namespace Defr\SocialiteModule\Account;

use Defr\SocialiteModule\Account\Contract\AccountInterface;
use Anomaly\Streams\Platform\Model\Socialite\SocialiteAccountsEntryModel;

class AccountModel extends SocialiteAccountsEntryModel implements AccountInterface
{

    /**
     * Gets the user.
     *
     * @return  UserInterface  The user.
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Gets the provider.
     *
     * @return  ProviderInterface  The provider.
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Gets the configuration attribute.
     *
     * @return  array
     */
    public function getConfigAttribute()
    {
        return unserialize(array_get($this->attributes, 'config', 'a:0:{}'));
    }

    /**
     * Sets the configuration attribute.
     *
     * @param   mixed  $config  The config
     * @return  $this
     */
    public function setConfigAttribute($config)
    {
        if (is_array($config)) {
            $config = serialize($config);
        }

        array_set($this->attributes, 'config', $config);

        return $this;
    }

}
