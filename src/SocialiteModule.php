<?php namespace Defr\SocialiteModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class SocialiteModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var  bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var  string
     */
    protected $icon = 'fa fa-puzzle-piece';

    /**
     * The module sections.
     *
     * @var  array
     */
    protected $sections = [
        'providers' => [
            'buttons' => [
                'new_provider',
            ],
        ],
        'accounts',
    ];

}
