<?php namespace Defr\SocialiteModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Model\Socialite\SocialiteAccountsEntryModel;
use Anomaly\Streams\Platform\Model\Socialite\SocialiteProvidersEntryModel;
use Defr\SocialiteModule\Account\AccountModel;
use Defr\SocialiteModule\Account\AccountRepository;
use Defr\SocialiteModule\Account\Contract\AccountRepositoryInterface;
use Defr\SocialiteModule\Provider\Contract\ProviderInterface;
use Defr\SocialiteModule\Provider\Contract\ProviderRepositoryInterface;
use Defr\SocialiteModule\Provider\ProviderModel;
use Defr\SocialiteModule\Provider\ProviderRepository;
use JhaoDa\SocialiteProviders\MailRu\MailRuExtendSocialite;
use JhaoDa\SocialiteProviders\Odnoklassniki\OdnoklassnikiExtendSocialite;
use SocialiteProviders\Manager\SocialiteWasCalled;
use SocialiteProviders\VKontakte\VKontakteExtendSocialite;

class SocialiteModuleServiceProvider extends AddonServiceProvider {

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/socialite/accounts'           => 'Defr\SocialiteModule\Http\Controller\Admin\AccountsController@index',
        'admin/socialite/accounts/create'    => 'Defr\SocialiteModule\Http\Controller\Admin\AccountsController@create',
        'admin/socialite/accounts/edit/{id}' => 'Defr\SocialiteModule\Http\Controller\Admin\AccountsController@edit',
        'admin/socialite'                    => 'Defr\SocialiteModule\Http\Controller\Admin\ProvidersController@index',
        'admin/socialite/create'             => 'Defr\SocialiteModule\Http\Controller\Admin\ProvidersController@create',
        'admin/socialite/edit/{id}'          => 'Defr\SocialiteModule\Http\Controller\Admin\ProvidersController@edit',
        'social-login/{slug}'                => 'Defr\SocialiteModule\Http\Controller\SocialController@login',
        'social-login/callback/{slug}'       => 'Defr\SocialiteModule\Http\Controller\SocialController@callback',
    ];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        SocialiteWasCalled::class => [
            VKontakteExtendSocialite::class,
            OdnoklassnikiExtendSocialite::class,
            MailRuExtendSocialite::class,
        ],
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        SocialiteAccountsEntryModel::class  => AccountModel::class,
        SocialiteProvidersEntryModel::class => ProviderModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        AccountRepositoryInterface::class  => AccountRepository::class,
        ProviderRepositoryInterface::class => ProviderRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        \SocialiteProviders\Manager\ServiceProvider::class,
    ];

    /**
     * Boot the addon.
     *
     * @param  ProviderRepositoryInterface  $providers  The providers
     */
    public function boot(ProviderRepositoryInterface $providers) {
        $providers->all()->enabled()->each(
            function (ProviderInterface $provider) {
                $config     = $provider->getConfig();
                $identifier = strtoupper($provider->getSlug());

                putenv("{$identifier}_KEY={$config['client_id']}");
                putenv("{$identifier}_SECRET={$config['client_secret']}");
                putenv("{$identifier}_REDIRECT_URI={$config['redirect']}");
            }
        );
    }

}
