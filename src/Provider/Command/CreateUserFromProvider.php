<?php namespace Defr\SocialiteModule\Provider\Command;

use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Defr\SocialiteModule\Account\Contract\AccountRepositoryInterface;
use Defr\SocialiteModule\Provider\Contract\ProviderInterface;

class CreateUserFromProvider
{

    /**
     * Provider entry
     *
     * @var ProviderInterface
     */
    protected $provider;

    /**
     * Create an instance of CreateUserFromProvider class
     *
     * @param  ProviderInterface  $provider  The provider
     */
    public function __construct(ProviderInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Handle the command
     *
     * @param   AccountRepositoryInterface  $accounts  The accounts
     * @param   UserRepositoryInterface     $users     The users
     * @return  UserInterface|null
     */
    public function handle(
        AccountRepositoryInterface $accounts,
        UserRepositoryInterface $users
    )
    {
        $account = $accounts->findByProvider($this->provider);

        return $users->create([
            'email'        => $account->getEmail(),
            'username'     => $account->getNickname(),
            'display_name' => $account->getName(),
        ]);
    }

}
