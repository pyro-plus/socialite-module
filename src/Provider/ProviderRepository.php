<?php namespace Defr\SocialiteModule\Provider;

use Defr\SocialiteModule\Provider\Contract\ProviderRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ProviderRepository extends EntryRepository implements ProviderRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ProviderModel
     */
    protected $model;

    /**
     * Create a new ProviderRepository instance.
     *
     * @param ProviderModel $model
     */
    public function __construct(ProviderModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find provider by its slug
     *
     * @param   string  $slug   The slug
     * @return  ProviderInterface
     */
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }

}
