<?php namespace Defr\SocialiteModule\Provider;

use Anomaly\Streams\Platform\Model\Socialite\SocialiteProvidersEntryModel;
use Defr\SocialiteModule\Provider\Contract\ProviderInterface;
use Illuminate\Contracts\Auth\Guard;
use Laravel\Socialite\Facades\Socialite;

class ProviderModel extends SocialiteProvidersEntryModel implements ProviderInterface
{

    /**
     * Gets the name.
     *
     * @return  string  The name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the slug.
     *
     * @return  string  The slug.
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Gets the redirect.
     *
     * @return  string  The redirect.
     */
    public function getRedirectResponse()
    {
        return Socialite::with($this->getSlug())->redirect();
    }

    /**
     * Gets the social driver
     *
     * @return  Provider
     */
    public function driver()
    {
        return Socialite::driver($this->getSlug());
    }

    /**
     * Gets the url.
     *
     * @return  string  The url.
     */
    public function getUrl()
    {
        return url("social-login/{$this->getSlug()}");
    }

    /**
     * Gets the configuration.
     *
     * @return  array  The configuration.
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Determines if enabled.
     *
     * @return  boolean  True if enabled, False otherwise.
     */
    public function isEnabled()
    {
        return (bool) $this->enabled;
    }

    /**
     * Gets the configuration attribute.
     *
     * @return  array
     */
    public function getConfigAttribute()
    {
        return unserialize(array_get($this->attributes, 'config', 'a:0:{}'));
    }

    /**
     * Sets the configuration attribute.
     *
     * @param   mixed  $config  The config
     * @return  $this
     */
    public function setConfigAttribute($config)
    {
        if (is_array($config)) {
            $config = serialize($config);
        }

        array_set($this->attributes, 'config', $config);

        return $this;
    }

}
