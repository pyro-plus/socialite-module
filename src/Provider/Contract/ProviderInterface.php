<?php namespace Defr\SocialiteModule\Provider\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface ProviderInterface extends EntryInterface
{

    /**
     * Gets the name.
     *
     * @return  string  The name.
     */
    public function getName();

    /**
     * Gets the slug.
     *
     * @return  string  The slug.
     */
    public function getSlug();

    /**
     * Gets the redirect.
     *
     * @return  string  The redirect.
     */
    public function getRedirectResponse();

    /**
     * Gets the configuration.
     *
     * @return  array  The configuration.
     */
    public function getConfig();

    /**
     * Determines if enabled.
     *
     * @return  boolean  True if enabled, False otherwise.
     */
    public function isEnabled();

    /**
     * Gets the configuration attribute.
     *
     * @return  array
     */
    public function getConfigAttribute();

    /**
     * Sets the configuration attribute.
     *
     * @param   mixed  $config  The config
     * @return  $this
     */
    public function setConfigAttribute($config);

}
