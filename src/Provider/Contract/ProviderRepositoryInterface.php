<?php namespace Defr\SocialiteModule\Provider\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ProviderRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Find provider by its slug
     *
     * @param   string  $slug   The slug
     * @return  ProviderInterface
     */
    public function findBySlug($slug);

}
