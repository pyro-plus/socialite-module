<?php namespace Defr\SocialiteModule\Provider\Form;

class ProviderFormSections
{

    /**
     * Handle the command
     *
     * @param  ProviderFormBuilder  $builder  The builder
     */
    public function handle(ProviderFormBuilder $builder)
    {
        $entry  = app($builder->getModel())->find($builder->getEntry());

        $builder->setSections([
            'provider' => [
                'title'  => 'defr.module.socialite::section.main',
                'fields' => [
                    'name',
                    'slug',
                    'icon',
                    'enabled',
                ],
            ],
            'config' => [
                'title'  => 'defr.module.socialite::section.config',
                'fields' => array_keys($entry->getConfig()),
            ],
        ]);
    }

}
