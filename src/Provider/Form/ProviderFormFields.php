<?php namespace Defr\SocialiteModule\Provider\Form;

use Laravel\Socialite\Facades\Socialite;

class ProviderFormFields
{

    /**
     * Handle the command
     *
     * @param  ProviderFormBuilder  $builder  The builder
     */
    public function handle(ProviderFormBuilder $builder)
    {
        $entry  = app($builder->getModel())->find($builder->getEntry());
        $config = $entry->getConfig();

        $builder->setFields(array_merge(
            ['*'],
            array_map(
                function ($field) use ($config) {
                    return [
                        'field' => $field,
                        'type'  => 'anomaly.field_type.text',
                        'label' => ucwords(str_humanize($field)),
                        'value' => array_get($config, $field, ''),
                    ];
                },
                array_keys($config)
            )
        ));
    }

}
