<?php namespace Defr\SocialiteModule\Provider\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class ProviderFormBuilder extends FormBuilder
{

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [
        'config',
    ];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'cancel',
    ];

    /**
     * Fires just before saving
     *
     * @param  ProviderFormBuilder  $builder  The builder
     */
    public function onSaving(ProviderFormBuilder $builder)
    {
        $fields = array_get($builder->getSections(), 'config.fields', []);
        $config = array_only($builder->getFormValues()->all(), $fields);

        foreach ($fields as $field) {
            $builder->disableFormField($field);
        }

        $builder->setFormEntryAttribute('config', $config);
    }

}
