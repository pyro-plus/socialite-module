<?php namespace Defr\SocialiteModule\Provider;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\SocialiteModule\Provider\Contract\ProviderRepositoryInterface;

class ProviderSeeder extends Seeder
{

    /**
     * Seeder data
     *
     * @var  array
     */
    protected $data = [
        [
            'name'    => 'VKontakte',
            'slug'    => 'vkontakte',
            'enabled' => true,
            'config'  => [
                'client_id'     => '',
                'client_secret' => '',
                'redirect'      => '',
            ],
        ],
        [
            'name'    => 'Odnoklassniki',
            'slug'    => 'odnoklassniki',
            'enabled' => true,
            'config'  => [
                'client_id'     => '',
                'client_secret' => '',
                'client_public' => '',
                'redirect'      => '',
            ],
        ],
        [
            'name'   => 'Mail.ru',
            'slug'   => 'mailru',
            'config' => [
                'client_id'     => '',
                'client_secret' => '',
                'redirect'      => '',
            ],
        ],
    ];

    /**
     * Social providers
     *
     * @var ProviderRepositoryInterface
     */
    protected $providers;

    /**
     * Create an instance fo ProviderSeeder class
     *
     * @param  ProviderRepositoryInterface  $providers  The providers
     */
    public function __construct(ProviderRepositoryInterface $providers)
    {
        $this->providers = $providers;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        foreach ($this->data as $item) {
            $this->providers->create($item);
        }
    }
}
