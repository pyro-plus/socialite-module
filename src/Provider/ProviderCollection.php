<?php namespace Defr\SocialiteModule\Provider;

use Anomaly\Streams\Platform\Entry\EntryCollection;
use Defr\SocialiteModule\Provider\Contract\ProviderInterface;

class ProviderCollection extends EntryCollection
{

    /**
     * Get enabled entries
     *
     * @return  ProviderCollection
     */
    public function enabled()
    {
        return $this->filter(
            function (ProviderInterface $provider) {
                return $provider->isEnabled();
            }
        );
    }

}
