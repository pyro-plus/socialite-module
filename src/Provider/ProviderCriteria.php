<?php namespace Defr\SocialiteModule\Provider;

use Anomaly\Streams\Platform\Entry\EntryCriteria;

class ProviderCriteria extends EntryCriteria
{

    /**
     * Get enabled only
     *
     * @return  self
     */
    public function enabled()
    {
        $this->query->where('enabled', true);

        return $this;
    }

}
