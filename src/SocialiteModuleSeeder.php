<?php namespace Defr\SocialiteModule;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\SocialiteModule\Provider\ProviderSeeder;

class SocialiteModuleSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        $this->call(ProviderSeeder::class);
    }

}
