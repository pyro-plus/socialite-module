<?php namespace Defr\SocialiteModule\Http\Controller\Admin;

use Defr\SocialiteModule\Provider\Form\ProviderFormBuilder;
use Defr\SocialiteModule\Provider\Table\ProviderTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ProvidersController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ProviderTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ProviderTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ProviderFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ProviderFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ProviderFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ProviderFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
