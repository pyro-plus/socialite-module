<?php namespace Defr\SocialiteModule\Http\Controller\Admin;

use Defr\SocialiteModule\Account\Form\AccountFormBuilder;
use Defr\SocialiteModule\Account\Table\AccountTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class AccountsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param AccountTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(AccountTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param AccountFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(AccountFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param AccountFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(AccountFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
