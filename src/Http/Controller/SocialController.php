<?php namespace Defr\SocialiteModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Defr\SocialiteModule\Account\Command\AuthenticateUser;
use Defr\SocialiteModule\Account\Command\FindOrCreateUser;
use Defr\SocialiteModule\Account\Command\GetProfile;
use Defr\SocialiteModule\Account\Contract\AccountRepositoryInterface;
use Defr\SocialiteModule\Provider\Contract\ProviderRepositoryInterface;

class SocialController extends PublicController
{

    /**
     * Login
     *
     * @param   ProviderRepositoryInterface  $providers  The providers
     * @param   string                       $slug       The slug
     * @return  Response
     */
    public function login(ProviderRepositoryInterface $providers, $slug)
    {
        /* @var ProviderInterface $provider */
        if (!$provider = $providers->findBySlug($slug)) {
            return $this->redirect->back();
        }

        return $provider->getRedirectResponse();
    }

    /**
     * Social login callback
     *
     * @param   ProviderRepositoryInterface  $providers  The providers
     * @param   AccountRepositoryInterface   $accounts   The accounts
     * @param   string                       $slug       The slug
     * @return  Response
     */
    public function callback(
        ProviderRepositoryInterface $providers,
        AccountRepositoryInterface $accounts,
        $slug
    )
    {
        /* @var ProviderInterface $provider */
        if (!$provider = $providers->findBySlug($slug)) {
            return $this->redirect->back();
        }

        /* @var Laravel\Socialite\Contracts\User $profile */
        $profile = $this->dispatch(new GetProfile($provider));

        /* @var UserInterface $user */
        if ($user = $this->dispatch(new FindOrCreateUser($profile))) {
            return $this->redirect->back();
        }

        /* @var AccountInterface $account */
        if (!$account = $accounts->findByUserAndProvider($user, $provider)) {
            $account = $accounts->create([
                'provider_id' => $provider->getId(),
                'user_id'     => $user->getId(),
                'config'      => $profile->getRaw(),
            ]);
        }

        $this->dispatch(new AuthenticateUser($user));

        return $this->redirect->intended('/');
    }

}
